// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'literature_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

LiteratureModel _$LiteratureModelFromJson(Map<String, dynamic> json) {
  return _LiteratureModel.fromJson(json);
}

/// @nodoc
mixin _$LiteratureModel {
  String get id => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $LiteratureModelCopyWith<LiteratureModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LiteratureModelCopyWith<$Res> {
  factory $LiteratureModelCopyWith(
          LiteratureModel value, $Res Function(LiteratureModel) then) =
      _$LiteratureModelCopyWithImpl<$Res, LiteratureModel>;
  @useResult
  $Res call({String id});
}

/// @nodoc
class _$LiteratureModelCopyWithImpl<$Res, $Val extends LiteratureModel>
    implements $LiteratureModelCopyWith<$Res> {
  _$LiteratureModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_LiteratureModelCopyWith<$Res>
    implements $LiteratureModelCopyWith<$Res> {
  factory _$$_LiteratureModelCopyWith(
          _$_LiteratureModel value, $Res Function(_$_LiteratureModel) then) =
      __$$_LiteratureModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String id});
}

/// @nodoc
class __$$_LiteratureModelCopyWithImpl<$Res>
    extends _$LiteratureModelCopyWithImpl<$Res, _$_LiteratureModel>
    implements _$$_LiteratureModelCopyWith<$Res> {
  __$$_LiteratureModelCopyWithImpl(
      _$_LiteratureModel _value, $Res Function(_$_LiteratureModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
  }) {
    return _then(_$_LiteratureModel(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_LiteratureModel implements _LiteratureModel {
  _$_LiteratureModel({required this.id});

  factory _$_LiteratureModel.fromJson(Map<String, dynamic> json) =>
      _$$_LiteratureModelFromJson(json);

  @override
  final String id;

  @override
  String toString() {
    return 'LiteratureModel(id: $id)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_LiteratureModel &&
            (identical(other.id, id) || other.id == id));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_LiteratureModelCopyWith<_$_LiteratureModel> get copyWith =>
      __$$_LiteratureModelCopyWithImpl<_$_LiteratureModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_LiteratureModelToJson(
      this,
    );
  }
}

abstract class _LiteratureModel implements LiteratureModel {
  factory _LiteratureModel({required final String id}) = _$_LiteratureModel;

  factory _LiteratureModel.fromJson(Map<String, dynamic> json) =
      _$_LiteratureModel.fromJson;

  @override
  String get id;
  @override
  @JsonKey(ignore: true)
  _$$_LiteratureModelCopyWith<_$_LiteratureModel> get copyWith =>
      throw _privateConstructorUsedError;
}

import 'package:data_api/export.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'literature_model.g.dart';

part 'literature_model.freezed.dart';

@freezed
class LiteratureModel with _$LiteratureModel implements WithId<String> {
  factory LiteratureModel({
    required String id,
  }) = _LiteratureModel;

  factory LiteratureModel.fromJson(Map<String, dynamic> json) =>
      _$LiteratureModelFromJson(json);
}

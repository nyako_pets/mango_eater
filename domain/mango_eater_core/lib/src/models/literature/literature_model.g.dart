// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'literature_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_LiteratureModel _$$_LiteratureModelFromJson(Map<String, dynamic> json) =>
    _$_LiteratureModel(
      id: json['id'] as String,
    );

Map<String, dynamic> _$$_LiteratureModelToJson(_$_LiteratureModel instance) =>
    <String, dynamic>{
      'id': instance.id,
    };

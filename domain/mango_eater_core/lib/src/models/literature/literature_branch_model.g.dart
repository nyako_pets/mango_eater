// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'literature_branch_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_LiteratureBranchModel _$$_LiteratureBranchModelFromJson(
        Map<String, dynamic> json) =>
    _$_LiteratureBranchModel(
      id: json['id'] as String,
      parentId: json['parentId'] as String,
    );

Map<String, dynamic> _$$_LiteratureBranchModelToJson(
        _$_LiteratureBranchModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'parentId': instance.parentId,
    };

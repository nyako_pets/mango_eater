// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'literature_chapter_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

LiteratureChapterModel _$LiteratureChapterModelFromJson(
    Map<String, dynamic> json) {
  return _LiteratureChapterModel.fromJson(json);
}

/// @nodoc
mixin _$LiteratureChapterModel {
  String get id => throw _privateConstructorUsedError;
  String get parentId => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $LiteratureChapterModelCopyWith<LiteratureChapterModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LiteratureChapterModelCopyWith<$Res> {
  factory $LiteratureChapterModelCopyWith(LiteratureChapterModel value,
          $Res Function(LiteratureChapterModel) then) =
      _$LiteratureChapterModelCopyWithImpl<$Res, LiteratureChapterModel>;
  @useResult
  $Res call({String id, String parentId});
}

/// @nodoc
class _$LiteratureChapterModelCopyWithImpl<$Res,
        $Val extends LiteratureChapterModel>
    implements $LiteratureChapterModelCopyWith<$Res> {
  _$LiteratureChapterModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? parentId = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      parentId: null == parentId
          ? _value.parentId
          : parentId // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_LiteratureChapterModelCopyWith<$Res>
    implements $LiteratureChapterModelCopyWith<$Res> {
  factory _$$_LiteratureChapterModelCopyWith(_$_LiteratureChapterModel value,
          $Res Function(_$_LiteratureChapterModel) then) =
      __$$_LiteratureChapterModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String id, String parentId});
}

/// @nodoc
class __$$_LiteratureChapterModelCopyWithImpl<$Res>
    extends _$LiteratureChapterModelCopyWithImpl<$Res,
        _$_LiteratureChapterModel>
    implements _$$_LiteratureChapterModelCopyWith<$Res> {
  __$$_LiteratureChapterModelCopyWithImpl(_$_LiteratureChapterModel _value,
      $Res Function(_$_LiteratureChapterModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? parentId = null,
  }) {
    return _then(_$_LiteratureChapterModel(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      parentId: null == parentId
          ? _value.parentId
          : parentId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_LiteratureChapterModel implements _LiteratureChapterModel {
  _$_LiteratureChapterModel({required this.id, required this.parentId});

  factory _$_LiteratureChapterModel.fromJson(Map<String, dynamic> json) =>
      _$$_LiteratureChapterModelFromJson(json);

  @override
  final String id;
  @override
  final String parentId;

  @override
  String toString() {
    return 'LiteratureChapterModel(id: $id, parentId: $parentId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_LiteratureChapterModel &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.parentId, parentId) ||
                other.parentId == parentId));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, parentId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_LiteratureChapterModelCopyWith<_$_LiteratureChapterModel> get copyWith =>
      __$$_LiteratureChapterModelCopyWithImpl<_$_LiteratureChapterModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_LiteratureChapterModelToJson(
      this,
    );
  }
}

abstract class _LiteratureChapterModel implements LiteratureChapterModel {
  factory _LiteratureChapterModel(
      {required final String id,
      required final String parentId}) = _$_LiteratureChapterModel;

  factory _LiteratureChapterModel.fromJson(Map<String, dynamic> json) =
      _$_LiteratureChapterModel.fromJson;

  @override
  String get id;
  @override
  String get parentId;
  @override
  @JsonKey(ignore: true)
  _$$_LiteratureChapterModelCopyWith<_$_LiteratureChapterModel> get copyWith =>
      throw _privateConstructorUsedError;
}

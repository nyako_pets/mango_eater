import 'package:data_api/export.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'literature_chapter_model.g.dart';

part 'literature_chapter_model.freezed.dart';

@freezed
class LiteratureChapterModel
    with _$LiteratureChapterModel
    implements WithId<String>, WithParentId<String> {
  factory LiteratureChapterModel({
    required String id,
    required String parentId,
  }) = _LiteratureChapterModel;

  factory LiteratureChapterModel.fromJson(Map<String, dynamic> json) =>
      _$LiteratureChapterModelFromJson(json);
}

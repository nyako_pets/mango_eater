// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'literature_branch_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

LiteratureBranchModel _$LiteratureBranchModelFromJson(
    Map<String, dynamic> json) {
  return _LiteratureBranchModel.fromJson(json);
}

/// @nodoc
mixin _$LiteratureBranchModel {
  String get id => throw _privateConstructorUsedError;
  String get parentId => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $LiteratureBranchModelCopyWith<LiteratureBranchModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LiteratureBranchModelCopyWith<$Res> {
  factory $LiteratureBranchModelCopyWith(LiteratureBranchModel value,
          $Res Function(LiteratureBranchModel) then) =
      _$LiteratureBranchModelCopyWithImpl<$Res, LiteratureBranchModel>;
  @useResult
  $Res call({String id, String parentId});
}

/// @nodoc
class _$LiteratureBranchModelCopyWithImpl<$Res,
        $Val extends LiteratureBranchModel>
    implements $LiteratureBranchModelCopyWith<$Res> {
  _$LiteratureBranchModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? parentId = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      parentId: null == parentId
          ? _value.parentId
          : parentId // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_LiteratureBranchModelCopyWith<$Res>
    implements $LiteratureBranchModelCopyWith<$Res> {
  factory _$$_LiteratureBranchModelCopyWith(_$_LiteratureBranchModel value,
          $Res Function(_$_LiteratureBranchModel) then) =
      __$$_LiteratureBranchModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String id, String parentId});
}

/// @nodoc
class __$$_LiteratureBranchModelCopyWithImpl<$Res>
    extends _$LiteratureBranchModelCopyWithImpl<$Res, _$_LiteratureBranchModel>
    implements _$$_LiteratureBranchModelCopyWith<$Res> {
  __$$_LiteratureBranchModelCopyWithImpl(_$_LiteratureBranchModel _value,
      $Res Function(_$_LiteratureBranchModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? parentId = null,
  }) {
    return _then(_$_LiteratureBranchModel(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      parentId: null == parentId
          ? _value.parentId
          : parentId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_LiteratureBranchModel implements _LiteratureBranchModel {
  _$_LiteratureBranchModel({required this.id, required this.parentId});

  factory _$_LiteratureBranchModel.fromJson(Map<String, dynamic> json) =>
      _$$_LiteratureBranchModelFromJson(json);

  @override
  final String id;
  @override
  final String parentId;

  @override
  String toString() {
    return 'LiteratureBranchModel(id: $id, parentId: $parentId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_LiteratureBranchModel &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.parentId, parentId) ||
                other.parentId == parentId));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, parentId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_LiteratureBranchModelCopyWith<_$_LiteratureBranchModel> get copyWith =>
      __$$_LiteratureBranchModelCopyWithImpl<_$_LiteratureBranchModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_LiteratureBranchModelToJson(
      this,
    );
  }
}

abstract class _LiteratureBranchModel implements LiteratureBranchModel {
  factory _LiteratureBranchModel(
      {required final String id,
      required final String parentId}) = _$_LiteratureBranchModel;

  factory _LiteratureBranchModel.fromJson(Map<String, dynamic> json) =
      _$_LiteratureBranchModel.fromJson;

  @override
  String get id;
  @override
  String get parentId;
  @override
  @JsonKey(ignore: true)
  _$$_LiteratureBranchModelCopyWith<_$_LiteratureBranchModel> get copyWith =>
      throw _privateConstructorUsedError;
}

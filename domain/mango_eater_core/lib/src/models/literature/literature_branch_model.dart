import 'package:data_api/export.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'literature_branch_model.g.dart';

part 'literature_branch_model.freezed.dart';

@freezed
class LiteratureBranchModel
    with _$LiteratureBranchModel
    implements WithId<String>, WithParentId<String> {
  factory LiteratureBranchModel({
    required String id,
    required String parentId,
  }) = _LiteratureBranchModel;

  factory LiteratureBranchModel.fromJson(Map<String, dynamic> json) =>
      _$LiteratureBranchModelFromJson(json);
}

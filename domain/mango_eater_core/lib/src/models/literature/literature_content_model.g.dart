// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'literature_content_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_LiteratureContentModel _$$_LiteratureContentModelFromJson(
        Map<String, dynamic> json) =>
    _$_LiteratureContentModel(
      id: json['id'] as String,
      parentId: json['parentId'] as String,
    );

Map<String, dynamic> _$$_LiteratureContentModelToJson(
        _$_LiteratureContentModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'parentId': instance.parentId,
    };

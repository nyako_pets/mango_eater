import 'package:data_api/export.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'literature_content_model.g.dart';

part 'literature_content_model.freezed.dart';

@freezed
class LiteratureContentModel
    with _$LiteratureContentModel
    implements WithId<String>, WithParentId<String> {
  factory LiteratureContentModel({
    required String id,
    required String parentId,
  }) = _LiteratureContentModel;

  factory LiteratureContentModel.fromJson(Map<String, dynamic> json) =>
      _$LiteratureContentModelFromJson(json);
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'literature_chapter_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_LiteratureChapterModel _$$_LiteratureChapterModelFromJson(
        Map<String, dynamic> json) =>
    _$_LiteratureChapterModel(
      id: json['id'] as String,
      parentId: json['parentId'] as String,
    );

Map<String, dynamic> _$$_LiteratureChapterModelToJson(
        _$_LiteratureChapterModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'parentId': instance.parentId,
    };

import 'package:data_api/export.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'image_model.g.dart';

part 'image_model.freezed.dart';

@freezed
class ImageModel
    with _$ImageModel
    implements WithId<String>, WithParentId<String> {
  factory ImageModel({
    required String id,
    // id обьекта, которому пренадлежит картинка
    required String parentId,

  }) = _ImageModel;

  factory ImageModel.fromJson(Map<String, dynamic> json) =>
      _$ImageModelFromJson(json);
}

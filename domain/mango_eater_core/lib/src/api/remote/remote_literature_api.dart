import 'package:data_api/export.dart';

import '../../models/literature/literature_model.dart';

abstract class RemoteLiteratureApi<T extends LiteratureModel> extends DataApi<T> with ById<String>, GetById {}

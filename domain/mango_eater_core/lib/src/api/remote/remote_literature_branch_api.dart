import 'package:data_api/export.dart';

import '../../models/literature/export.dart';

abstract class RemoteLiteratureBranchApi<T extends LiteratureBranchModel>
    extends DataApi<T> with ByParentId<String>, GetByParentId {}

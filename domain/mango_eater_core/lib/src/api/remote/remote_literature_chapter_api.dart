import 'package:data_api/export.dart';

import '../../models/literature/literature_chapter_model.dart';

abstract class RemoteLiteratureChapterApiArg {}

abstract class RemoteLiteratureChapterApiResult<T extends LiteratureChapterModel> extends ByArgResult<T> {
  List<T> get items;
}

abstract class RemoteLiteratureChapterApi<T extends LiteratureChapterModel, A extends RemoteLiteratureChapterApiArg,
    R extends RemoteLiteratureChapterApiResult<T>> extends DataApi<T> with ByArg<A, R, T>, GetByArg {}

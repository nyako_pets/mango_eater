import 'package:data_api/export.dart';

import '../../models/literature/literature_content_model.dart';

abstract class RemoteLiteratureContentApiArg {}

abstract class RemoteLiteratureContentApiResult<T extends LiteratureContentModel> extends ByArgResult<T> {
  List<T> get items;
}

abstract class RemoteLiteratureContentApi<T extends LiteratureContentModel, A extends RemoteLiteratureContentApiArg,
    R extends RemoteLiteratureContentApiResult<T>> extends DataApi<T> with ByArg<A, R, T>, GetByArg {}

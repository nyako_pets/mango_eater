import 'package:data_api/export.dart';

import '../../models/literature/export.dart';

abstract class LocalLiteratureBranchApi<T extends LiteratureBranchModel>
    extends DataApi<T>
    with ByParentId<String>, GetByParentId, ListenByParentId, SaveAll {}

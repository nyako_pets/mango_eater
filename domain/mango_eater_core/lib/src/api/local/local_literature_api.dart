import 'package:data_api/export.dart';

import '../../models/literature/literature_model.dart';

abstract class LocalLiteratureApi<T extends LiteratureModel> extends DataApi<T>
    with ById<String>, Save, GetOptionalById, ListenOptionalById {

}

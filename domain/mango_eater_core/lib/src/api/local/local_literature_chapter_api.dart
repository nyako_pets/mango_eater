import 'package:data_api/export.dart';

import '../../models/literature/literature_chapter_model.dart';

abstract class LocalLiteratureChapterApiArg {}

abstract class LocalLiteratureChapterApiResult<T extends LiteratureChapterModel> extends ByArgResult<T> {
  List<T> get items;
}

abstract class LocalLiteratureChapterApi<T extends LiteratureChapterModel, A extends LocalLiteratureChapterApiArg,
    R extends LocalLiteratureChapterApiResult<T>> extends DataApi<T> with ByArg<A, R, T>, GetByArg, ListenByArg, SaveAll {}

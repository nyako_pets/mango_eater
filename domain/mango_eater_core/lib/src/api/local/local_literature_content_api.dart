import 'package:data_api/export.dart';

import '../../models/literature/literature_content_model.dart';

abstract class LocalLiteratureContentApiArg {}

abstract class LocalLiteratureContentApiResult<T extends LiteratureContentModel> extends ByArgResult<T> {
  List<T> get items;
}

abstract class LocalLiteratureContentApi<T extends LiteratureContentModel, A extends LocalLiteratureContentApiArg,
    R extends LocalLiteratureContentApiResult<T>> extends DataApi<T> with ByArg<A, R, T>, GetByArg, ListenByArg, SaveAll {}

import 'package:mango_eater_core/mango_eater_core.dart';
import 'package:mango_eater_use_cases/mango_eater_use_cases.dart';

class LiteratureViewLogic extends Disposable {
  final LiteratureWorker _literatureWorker;
  final LiteratureModel initialValue;

  LiteratureViewLogic(
    this._literatureWorker,
    this.initialValue,
  );

  Stream<LiteratureModel> listenLiterature() async* {
    yield initialValue;
    yield* _literatureWorker.listenCached(initialValue.id);
  }

  @override
  void dispose() {

  }
}

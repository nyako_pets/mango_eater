import 'package:flutter/material.dart';
import 'package:mango_eater_use_cases/mango_eater_use_cases.dart';

/// может быть сразу 3 значение
typedef ErrorWithValueCallback<T> = Function(bool isLoading, T? value, Object? error);

class ErrorWithValueBuilder<T> extends StatelessWidget {
  final AsyncSnapshot<T> snapshot;
  final ErrorWithValueCallback<T> builder;

  const ErrorWithValueBuilder({
    required this.snapshot,
    required this.builder,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    T? data = snapshot.data;
    final error = snapshot.error;
    if (error is ErrorWithResult) {
      final resultFromError = error.result;
      if (resultFromError is T) {
        data = resultFromError;
      }
    }
    final isLoading = snapshot.connectionState == ConnectionState.waiting;
    return builder(isLoading, data, error);
  }
}

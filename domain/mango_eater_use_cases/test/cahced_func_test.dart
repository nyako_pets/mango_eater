import 'package:core/export.dart';
import 'package:data_api/export.dart';
import 'package:test/test.dart';
import 'package:mango_eater_use_cases/mango_eater_use_cases.dart';
import 'package:collection/collection.dart';

const _model1 = _Model('1');
const _model2 = _Model('2');
const _model3 = _Model('3');
const _model4 = _Model('4');
const _model5 = _Model('5');
const _model6 = _Model('6');

main() async {
  test('description', () async {
    await _getCachedTestCase(
      remoteStorage: {_model1},
      expectLocalStorage: {_model1},
    );
    await _getCachedTestCase(
        localStorage: {_model1},
        errorExpect: (error) {
          expect(error is ErrorWithResult, true);
          expect((error as ErrorWithResult).result, _model1);
        });
  });
}

Future<void> _getCachedTestCase({
  Set<_Model>? localStorage,
  Set<_Model>? remoteStorage,
  Set<_Model>? expectLocalStorage,
  Set<_Model>? expectRemoteStorage,
  _Model requestedModel = _model1,
  _Model expectedModel = _model1,
  void Function(Object)? errorExpect,
}) async {
  localStorage ??= {};
  remoteStorage ??= {};
  final remoteApi = _RemoteApi(remoteStorage);
  final localApi = _LocalApi(localStorage);
  try {
    final result = await GetCachedByIdFunc(
      remoteGetter: remoteApi,
      localSave: localApi,
      localGetter: localApi,
    )(requestedModel.id);
    await Future(() {});
    final listEquality = const SetEquality();
    expect(result, expectedModel);
    if (expectLocalStorage != null) {
      expect(listEquality.equals(localStorage, expectLocalStorage), true);
    }
    if (expectRemoteStorage != null) {
      expect(listEquality.equals(remoteStorage, expectRemoteStorage), true);
    }
    if (errorExpect != null) {
      throw 'errorExpect != null and error is null';
    }
  } catch (e) {
    if (errorExpect != null) {
      errorExpect.call(e);
    } else {
      rethrow;
    }
  }
}

@immutable
class _Model {
  final String id;

  const _Model(this.id);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is _Model && runtimeType == other.runtimeType && id == other.id;

  @override
  int get hashCode => id.hashCode;
}

class _LocalApi extends DataApi<_Model>
    with ById<String>, GetOptionalById, Save {
  final Set<_Model> localStorage;

  _LocalApi(this.localStorage);

  @override
  Future<void> save(_Model model) async {
    localStorage.add(model);
  }

  @override
  Future<_Model?> getOptionalById(String id) async {
    return localStorage.firstWhereOrNull((element) => element.id == id);
  }
}

class _RemoteApi extends DataApi<_Model> with ById<String>, GetById {
  final Set<_Model> remoteStorage;

  _RemoteApi(this.remoteStorage);

  @override
  Future<_Model> getById(String id) async {
    return remoteStorage.firstWhere((element) => element.id == id);
  }
}

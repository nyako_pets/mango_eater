import 'package:mango_eater_core/mango_eater_core.dart';

import '../../mango_eater_use_cases.dart';

class LiteratureWorker {
  final LocalLiteratureApi _localLiteratureApi;
  final RemoteLiteratureApi _remoteLiteratureApi;

  LiteratureWorker(
    this._localLiteratureApi,
    this._remoteLiteratureApi,
  );

  late final getCached = GetCachedByIdFunc<String, LiteratureModel>(
    remoteGetter: _remoteLiteratureApi,
    localSave: _localLiteratureApi,
    localGetter: _localLiteratureApi,
  );

  late final listenCached = ListenCachedByIdFunc<String, LiteratureModel>(
    getCachedFunc: getCached,
    localListen: _localLiteratureApi,
  );
}

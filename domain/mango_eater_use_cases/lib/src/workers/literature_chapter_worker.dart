import 'package:mango_eater_core/mango_eater_core.dart';

import '../../mango_eater_use_cases.dart';

class LiteratureChapterWorker<
    T extends LiteratureChapterModel,
    LA extends LocalLiteratureChapterApiArg,
    LR extends LocalLiteratureChapterApiResult<T>,
    RA extends RemoteLiteratureChapterApiArg,
    RR extends RemoteLiteratureChapterApiResult<T>> {
  final LocalLiteratureChapterApi<T, LA, LR> _localLiteratureChapterApi;
  final RemoteLiteratureChapterApi<T, RA, RR> _remoteLiteratureChapterApi;

  LiteratureChapterWorker(
    this._localLiteratureChapterApi,
    this._remoteLiteratureChapterApi,
  );

  late final getByArgCached = GetCachedByArgFunc<T, LA, LR, RA, RR>(
    remoteGetter: _remoteLiteratureChapterApi,
    localSave: _localLiteratureChapterApi,
    localGetter: _localLiteratureChapterApi,
  );

  late final listenByArgCached = ListenCachedByArgFunc<T, LA, LR, RA, RR>(
    getCachedByArgFunc: getByArgCached,
    localListenByArg: _localLiteratureChapterApi,
  );
}

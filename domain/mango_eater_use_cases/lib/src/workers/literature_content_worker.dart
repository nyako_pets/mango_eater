import 'package:mango_eater_core/mango_eater_core.dart';

import '../../mango_eater_use_cases.dart';

class LiteratureContentWorker<
    T extends LiteratureContentModel,
    LA extends LocalLiteratureContentApiArg,
    LR extends LocalLiteratureContentApiResult<T>,
    RA extends RemoteLiteratureContentApiArg,
    RR extends RemoteLiteratureContentApiResult<T>> {
  final LocalLiteratureContentApi _localLiteratureContentApi;
  final RemoteLiteratureContentApi _remoteLiteratureContentApi;

  LiteratureContentWorker(
    this._localLiteratureContentApi,
    this._remoteLiteratureContentApi,
  );

  late final getByArgCached = GetCachedByArgFunc<LiteratureContentModel, LocalLiteratureContentApiArg, LocalLiteratureContentApiResult,
      RemoteLiteratureContentApiArg, RemoteLiteratureContentApiResult>(
    remoteGetter: _remoteLiteratureContentApi,
    localSave: _localLiteratureContentApi,
    localGetter: _localLiteratureContentApi,
  );

  late final listenByArgCached = ListenCachedByArgFunc<LiteratureContentModel, LocalLiteratureContentApiArg,
      LocalLiteratureContentApiResult, RemoteLiteratureContentApiArg, RemoteLiteratureContentApiResult>(
    getCachedByArgFunc: getByArgCached,
    localListenByArg: _localLiteratureContentApi,
  );
}

import 'package:mango_eater_core/mango_eater_core.dart';

import '../functions/get_cached_by_parent_id.dart';
import '../functions/listen_cached_by_parent_id.dart';

class LiteratureBranchWorker {
  final LocalLiteratureBranchApi _localLiteratureBranchApi;
  final RemoteLiteratureBranchApi _remoteLiteratureBranchApi;

  LiteratureBranchWorker(
    this._localLiteratureBranchApi,
    this._remoteLiteratureBranchApi,
  );

  late final getByParentCached = GetCachedByParentIdFunc<String, LiteratureBranchModel>(
    remoteGetter: _remoteLiteratureBranchApi,
    localSave: _localLiteratureBranchApi,
    localGetter: _localLiteratureBranchApi,
  );

  late final listenByParentCached = ListenCachedByParentIdFunc<String, LiteratureBranchModel>(
    getCachedByParentIdFunc: getByParentCached,
    localListen: _localLiteratureBranchApi,
  );
}

import 'dart:async';

import 'package:data_api/export.dart';

import '../exceptions/local_getter_error.dart';
import '../exceptions/local_save_error.dart';
import '../exceptions/remote_getter_error.dart';

class GetCachedByParentIdFunc<ID, T> {
  final GetByParentId<ID, T> localGetter;
  final GetByParentId<ID, T> remoteGetter;
  final SaveAll<T> localSave;

  GetCachedByParentIdFunc({
    required this.remoteGetter,
    required this.localSave,
    required this.localGetter,
  });

  Future<List<T>> call(ID id) async {
    final remoteModel = await _getRemoteModels(id);
    unawaited(localSave.saveAll(remoteModel).catchError((e, s) {
      throw LocalSaveError(e, s);
    }));
    return remoteModel;
  }

  Future<List<T>> _getRemoteModels(ID id) async {
    try {
      return await remoteGetter.getByParentId(id);
    } catch (e, s) {
      final localModels = await _getLocalModels(id);
      return RemoteGetterErrorWithLocalModel<List<T>>(e, s, localModels).toReturn();
    }
  }

  Future<List<T>> _getLocalModels(ID id) async {
    try {
      return await localGetter.getByParentId(id);
    } catch (e, s) {
      throw LocalGetterError(e, s);
    }
  }
}

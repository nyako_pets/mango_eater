import 'package:core/export.dart';
import 'package:data_api/export.dart';

import '../../mango_eater_use_cases.dart';
import 'get_cached_by_parent_id.dart';

class ListenCachedByParentIdFunc<ID, T> {
  final ListenByParentId<ID, T> localListen;
  final GetCachedByParentIdFunc<ID, T> getCachedByParentIdFunc;

  ListenCachedByParentIdFunc({
    required this.localListen,
    required this.getCachedByParentIdFunc,
  });

  Stream<List<T>> call(ID id) {
    return getCachedByParentIdFunc(id).asStream().onErrorResume((error, stackTrace) async* {
      yield* Stream.error(error, stackTrace);
      yield* localListen.listenByParentId(id).switchMap<List<T>>((value) {
        return Stream.value(ErrorWithResult(error, stackTrace, value).toReturn());
      });
    }).switchMap((cachedModel) async* {
      yield cachedModel;
      yield* localListen.listenByParentId(id);
    });
  }
}

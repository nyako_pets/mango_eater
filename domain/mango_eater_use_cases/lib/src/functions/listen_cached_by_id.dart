import 'package:core/export.dart';
import 'package:data_api/export.dart';

import '../../mango_eater_use_cases.dart';

import 'get_cached_by_id.dart';

class ListenCachedByIdFunc<ID, T> {
  final ListenOptionalById<ID, T> localListen;
  final GetCachedByIdFunc<ID, T> getCachedFunc;

  ListenCachedByIdFunc({
    required this.localListen,
    required this.getCachedFunc,
  });

  Stream<T> call(ID id) {
    return getCachedFunc(id).asStream().onErrorResume((error, stackTrace) async* {
      yield* Stream.error(error, stackTrace);
      yield* localListen.listenOptionalById(id).notNull().switchMap<T>((value) {
        return Stream.value(ErrorWithResult(error, stackTrace, value).toReturn());
      });
    }).switchMap((cachedModel) async* {
      yield cachedModel;
      yield* localListen.listenOptionalById(id).notNull();
    });
  }
}

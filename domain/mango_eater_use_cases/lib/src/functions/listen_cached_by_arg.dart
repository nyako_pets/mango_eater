import 'package:core/export.dart';
import 'package:data_api/export.dart';

import '../../mango_eater_use_cases.dart';

class ListenCachedByArgFunc<T, LA, LR extends ByArgResult<T>, RA, RR extends ByArgResult<T>> {
  final GetCachedByArgFunc<T, LA, LR, RA, RR> getCachedByArgFunc;
  final ListenByArg<LA, LR, T> localListenByArg;

  ListenCachedByArgFunc({
    required this.getCachedByArgFunc,
    required this.localListenByArg,
  });

  Stream<Iterable<T>> call(
    LA localArg,
    RA remoteArg,
  ) {
    return getCachedByArgFunc(localArg, remoteArg).asStream().onErrorResume((error, stackTrace) async* {
      yield* Stream.error(error, stackTrace);
      yield* localListenByArg.listenByArg(localArg).switchMap<Iterable<T>>((value) {
        return Stream.value(
          ErrorWithResult(error, stackTrace, value.items).toReturn(),
        );
      });
    }).switchMap((cachedModel) async* {
      yield cachedModel;
      yield* localListenByArg.listenByArg(localArg).map((event) => event.items);
    });
  }
}

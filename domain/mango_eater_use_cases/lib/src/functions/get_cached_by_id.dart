import 'dart:async';

import 'package:data_api/export.dart';

import '../exceptions/local_getter_error.dart';
import '../exceptions/local_save_error.dart';
import '../exceptions/remote_getter_error.dart';

class GetCachedByIdFunc<ID, T> {
  final GetOptionalById<ID, T> localGetter;
  final GetById<ID, T> remoteGetter;
  final Save<T> localSave;

  GetCachedByIdFunc({
    required this.remoteGetter,
    required this.localSave,
    required this.localGetter,
  });

  Future<T> call(ID id) async {
    final remoteModel = await _getRemoteModel(id);
    unawaited(localSave.save(remoteModel).catchError((e, s) {
      throw LocalSaveError(e, s);
    }));
    return remoteModel;
  }

  Future<T> _getRemoteModel(ID id) async {
    try {
      return await remoteGetter.getById(id);
    } catch (e, s) {
      final localModel = await _getLocalModel(id);
      if (localModel != null) {
        return RemoteGetterErrorWithLocalModel<T>(e, s, localModel).toReturn();
      }
      throw RemoteGetterError(e, s);
    }
  }

  Future<T?> _getLocalModel(ID id) async {
    try {
      return await localGetter.getOptionalById(id);
    } catch (e, s) {
      throw LocalGetterError(e, s);
    }
  }
}

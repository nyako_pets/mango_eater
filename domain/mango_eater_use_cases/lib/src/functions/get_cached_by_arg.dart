import 'dart:async';

import 'package:data_api/export.dart';

import '../exceptions/local_getter_error.dart';
import '../exceptions/local_save_error.dart';
import '../exceptions/remote_getter_error.dart';

class GetCachedByArgFunc<T, LA, LR extends ByArgResult<T>, RA, RR extends ByArgResult<T>> {
  final GetByArg<LA, LR, T> localGetter;
  final GetByArg<RA, RR, T> remoteGetter;
  final SaveAll<T> localSave;

  GetCachedByArgFunc({
    required this.remoteGetter,
    required this.localSave,
    required this.localGetter,
  });

  Future<Iterable<T>> call(
    LA localArg,
    RA remoteArg,
  ) async {
    final remoteModel = await _getRemoteModels(
      remoteArg,
      localArg,
    );
    unawaited(localSave.saveAll(remoteModel).catchError((e, s) {
      throw LocalSaveError(e, s);
    }));
    return remoteModel;
  }

  Future<Iterable<T>> _getRemoteModels(RA rpa, LA lpa) async {
    try {
      final result = await remoteGetter.getByArg(rpa);
      return result.items;
    } catch (e, s) {
      final localModels = await _getLocalModels(lpa);
      return RemoteGetterErrorWithLocalModel<Iterable<T>>(e, s, localModels.items).toReturn();
    }
  }

  Future<LR> _getLocalModels(LA lpa) async {
    try {
      return await localGetter.getByArg(lpa);
    } catch (e, s) {
      throw LocalGetterError(e, s);
    }
  }
}

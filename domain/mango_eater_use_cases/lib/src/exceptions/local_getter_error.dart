import 'package:mango_eater_use_cases/src/exceptions/supported_error.dart';

class LocalGetterError<T> extends SupportedError<T> {
  LocalGetterError(super.error, super.stackTrace);
}

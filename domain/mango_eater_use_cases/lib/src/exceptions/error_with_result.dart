import 'supported_error.dart';

class ErrorWithResult<T> extends SupportedError<T> {
  final T result;

  ErrorWithResult._(super.error, super.stackTrace, this.result);

  factory ErrorWithResult(Object error, StackTrace stackTrace, T result) {
    if (error is ErrorWithResult<T>) {
      return error.cloneWithErrorWithNewValue(result);
    }
    return ErrorWithResult(error, stackTrace, result);
  }

  ErrorWithResult<T> cloneWithErrorWithNewValue(T value) {
    return ErrorWithResult._(error, stackTrace, value);
  }
}

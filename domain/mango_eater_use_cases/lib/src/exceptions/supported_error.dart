class SupportedError<T> extends Error {
  final Object error;
  final StackTrace stackTrace;

  SupportedError(this.error, this.stackTrace);

  Object get reason {
    final error = this.error;
    if (error is SupportedError) {
      return error.reason;
    } else {
      return error;
    }
  }

  StackTrace get reasonStackTrace {
    final error = this.error;
    if (error is SupportedError) {
      return error.reasonStackTrace;
    } else {
      return stackTrace;
    }
  }

  /// что бы не кидать ошибку, а возвращать её
  /// тогда будет проверка типа возвращаемого значения функции и SupportedError
  T toReturn() {
    throw this;
  }
}

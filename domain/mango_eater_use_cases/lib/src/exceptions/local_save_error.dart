import 'package:mango_eater_use_cases/src/exceptions/supported_error.dart';

class LocalSaveError<T> extends SupportedError {
  LocalSaveError(super.error, super.stackTrace);
}

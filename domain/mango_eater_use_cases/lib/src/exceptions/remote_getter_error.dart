import 'package:mango_eater_use_cases/src/exceptions/supported_error.dart';

import 'error_with_result.dart';

class RemoteGetterError<T> extends SupportedError<T> {
  RemoteGetterError(super.error, super.stackTrace);
}

class RemoteGetterErrorWithLocalModel<T> extends RemoteGetterError<T>
    implements ErrorWithResult<T> {
  @override
  final T result;

  RemoteGetterErrorWithLocalModel(
    super.error,
    super.stackTrace,
    this.result,
  );

  @override
  ErrorWithResult<T> cloneWithErrorWithNewValue(T value) {
    return RemoteGetterErrorWithLocalModel(error, stackTrace, value);
  }
}

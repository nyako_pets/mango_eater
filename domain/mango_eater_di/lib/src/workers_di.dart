import 'package:mango_eater_use_cases/mango_eater_use_cases.dart';

import 'core_di.dart';

class MangoEaterWorkersDi {
  final LiteratureWorker literatureWorker;
  final LiteratureBranchWorker literatureBranchWorker;
  final LiteratureChapterWorker literatureChapterWorker;
  final LiteratureContentWorker literatureContentWorker;

  MangoEaterWorkersDi(
    MangoEaterCoreDi core,
  )   : literatureWorker = LiteratureWorker(
          core.localLiteratureApi,
          core.remoteLiteratureApi,
        ),
        literatureBranchWorker = LiteratureBranchWorker(
          core.localLiteratureBranchApi,
          core.remoteLiteratureBranchApi,
        ),
        literatureChapterWorker = LiteratureChapterWorker(
          core.localLiteratureChapterApi,
          core.remoteLiteratureChapterApi,
        ),
        literatureContentWorker = LiteratureContentWorker(
          core.localLiteratureContentApi,
          core.remoteLiteratureContentApi,
        );
}

import 'package:mango_eater_core/mango_eater_core.dart';

class MangoEaterCoreDi<
    LiteratureModelG extends LiteratureModel,
    LiteratureBranchModelG extends LiteratureBranchModel,
    LiteratureChapterModelG extends LiteratureChapterModel,
    LiteratureContentModelG extends LiteratureContentModel,
    LocalLiteratureChapterApiArgG extends LocalLiteratureChapterApiArg,
    LocalLiteratureChapterApiResultG extends LocalLiteratureChapterApiResult<LiteratureChapterModelG>,
    RemoteLiteratureChapterApiArgG extends RemoteLiteratureChapterApiArg,
    RemoteLiteratureChapterApiResultG extends RemoteLiteratureChapterApiResult<LiteratureChapterModelG>,
    LocalLiteratureContentApiArgG extends LocalLiteratureContentApiArg,
    LocalLiteratureContentApiResultG extends LocalLiteratureContentApiResult<LiteratureContentModelG>,
    RemoteLiteratureContentApiArgG extends RemoteLiteratureContentApiArg,
    RemoteLiteratureContentApiResultG extends RemoteLiteratureContentApiResult<LiteratureContentModelG>> {
  //local
  final LocalLiteratureApi<LiteratureModelG> localLiteratureApi;
  final LocalLiteratureBranchApi<LiteratureBranchModelG> localLiteratureBranchApi;
  final LocalLiteratureChapterApi<LiteratureChapterModelG, LocalLiteratureChapterApiArgG, LocalLiteratureChapterApiResultG>
      localLiteratureChapterApi;
  final LocalLiteratureContentApi<LiteratureContentModelG, LocalLiteratureContentApiArgG, LocalLiteratureContentApiResultG>
      localLiteratureContentApi;

  //remote
  final RemoteLiteratureApi<LiteratureModelG> remoteLiteratureApi;
  final RemoteLiteratureBranchApi<LiteratureBranchModelG> remoteLiteratureBranchApi;
  final RemoteLiteratureChapterApi<LiteratureChapterModelG, RemoteLiteratureChapterApiArgG, RemoteLiteratureChapterApiResultG>
      remoteLiteratureChapterApi;
  final RemoteLiteratureContentApi<LiteratureContentModelG, RemoteLiteratureContentApiArgG, RemoteLiteratureContentApiResultG>
      remoteLiteratureContentApi;

  MangoEaterCoreDi(
    this.localLiteratureApi,
    this.localLiteratureBranchApi,
    this.localLiteratureChapterApi,
    this.localLiteratureContentApi,
    this.remoteLiteratureApi,
    this.remoteLiteratureBranchApi,
    this.remoteLiteratureChapterApi,
    this.remoteLiteratureContentApi,
  );
}

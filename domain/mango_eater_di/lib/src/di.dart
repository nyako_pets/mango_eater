import 'package:mango_eater_core/mango_eater_core.dart';
import 'package:mango_eater_di/src/core_di.dart';
import 'package:mango_eater_di/src/workers_di.dart';

class MangoEaterDI<
    LiteratureModelG extends LiteratureModel,
    LiteratureBranchModelG extends LiteratureBranchModel,
    LiteratureChapterModelG extends LiteratureChapterModel,
    LiteratureContentModelG extends LiteratureContentModel,
    LocalLiteratureChapterApiArgG extends LocalLiteratureChapterApiArg,
    LocalLiteratureChapterApiResultG extends LocalLiteratureChapterApiResult<LiteratureChapterModelG>,
    RemoteLiteratureChapterApiArgG extends RemoteLiteratureChapterApiArg,
    RemoteLiteratureChapterApiResultG extends RemoteLiteratureChapterApiResult<LiteratureChapterModelG>,
    LocalLiteratureContentApiArgG extends LocalLiteratureContentApiArg,
    LocalLiteratureContentApiResultG extends LocalLiteratureContentApiResult<LiteratureContentModelG>,
    RemoteLiteratureContentApiArgG extends RemoteLiteratureContentApiArg,
    RemoteLiteratureContentApiResultG extends RemoteLiteratureContentApiResult<LiteratureContentModelG>> {
  final MangoEaterCoreDi<
      LiteratureModelG,
      LiteratureBranchModelG,
      LiteratureChapterModelG,
      LiteratureContentModelG,
      LocalLiteratureChapterApiArgG,
      LocalLiteratureChapterApiResultG,
      RemoteLiteratureChapterApiArgG,
      RemoteLiteratureChapterApiResultG,
      LocalLiteratureContentApiArgG,
      LocalLiteratureContentApiResultG,
      RemoteLiteratureContentApiArgG,
      RemoteLiteratureContentApiResultG> core;
  final MangoEaterWorkersDi worker;

  MangoEaterDI(
    this.core,
  ) : worker = MangoEaterWorkersDi(core);
}

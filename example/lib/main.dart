import 'package:flutter/material.dart';
import 'package:mango_eater_di/mango_eater_di.dart';

import 'api_impl/local/literature_api.dart';
import 'api_impl/local/literature_branch_api.dart';
import 'api_impl/local/literature_chapter_api.dart';
import 'api_impl/local/literature_content_api.dart';
import 'api_impl/remote/literature_api.dart';
import 'api_impl/remote/literature_branch_api.dart';
import 'api_impl/remote/literature_chapter_api.dart';
import 'api_impl/remote/literature_content_api.dart';

void main() async {
  final mangoEaterDI = MangoEaterDI(
    MangoEaterCoreDi(
      LocalLiteratureApiImpl(),
      LocalLiteratureBranchApiImpl(),
      LocalLiteratureChapterApiImpl(),
      LocalLiteratureContentApiImpl(),
      RemoteLiteratureApiImpl(),
      RemoteLiteratureBranchApiImpl(),
      RemoteLiteratureChapterApiImpl(),
      RemoteLiteratureContentApiImpl(),
    ),
  );
  runApp(const MyApp());
  final literature = await mangoEaterDI.worker.literatureWorker.getCached.call('1');
  print(literature);
  final literatureBranches = await mangoEaterDI.worker.literatureBranchWorker.getByParentCached.call(literature.id);
  print(literatureBranches);
  final literatureChapters = await mangoEaterDI.worker.literatureChapterWorker.getByArgCached.call(
    LocalLiteratureChapterApiArgImpl(),
    RemoteLiteratureChapterApiArgImpl(),
  );
  print(literatureChapters);
  final literatureContent = await mangoEaterDI.worker.literatureContentWorker.getByArgCached.call(
    LocalLiteratureContentApiArgImpl(),
    RemoteLiteratureContentApiArgImpl(),
  );
  print(literatureContent);
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(

    );
  }
}

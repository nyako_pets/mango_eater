import 'package:mango_eater_core/mango_eater_core.dart';

import '../fake_data.dart';

class LocalLiteratureApiImpl extends LocalLiteratureApi {
  @override
  Future<LiteratureModel?> getOptionalById(String id) async {
    return FakeData.fakeLiterature;
  }

  @override
  Stream<LiteratureModel?> listenOptionalById(String id) {
    return Stream.value(FakeData.fakeLiterature);
  }

  @override
  Future<void> save(LiteratureModel model) async {}
}
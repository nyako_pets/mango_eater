import 'package:mango_eater_core/mango_eater_core.dart';

import '../fake_data.dart';

class LocalLiteratureChapterApiResultImpl extends LocalLiteratureChapterApiResult {
  @override
  final List<LiteratureChapterModel> items;

  LocalLiteratureChapterApiResultImpl(this.items);
}

class LocalLiteratureChapterApiArgImpl extends LocalLiteratureChapterApiArg {}

class LocalLiteratureChapterApiImpl extends LocalLiteratureChapterApi {
  @override
  Future<LocalLiteratureChapterApiResult<LiteratureChapterModel>> getByArg(LocalLiteratureChapterApiArg argument) async {
    return LocalLiteratureChapterApiResultImpl([FakeData.fakeChapter]);
  }

  @override
  Stream<LocalLiteratureChapterApiResult<LiteratureChapterModel>> listenByArg(LocalLiteratureChapterApiArg argument) {
    return Stream.value(LocalLiteratureChapterApiResultImpl([FakeData.fakeChapter]));
  }

  @override
  Future<void> saveAll(Iterable<LiteratureChapterModel> models) async {}
}

import 'package:mango_eater_core/mango_eater_core.dart';

import '../fake_data.dart';

class LocalLiteratureBranchApiImpl extends LocalLiteratureBranchApi {
  @override
  Future<List<LiteratureBranchModel>> getByParentId(String id) async {
    return [FakeData.fakeBranch];
  }

  @override
  Stream<List<LiteratureBranchModel>> listenByParentId(String id) {
    return Stream.value([FakeData.fakeBranch]);
  }

  @override
  Future<void> saveAll(Iterable<LiteratureBranchModel> models) async {}
}

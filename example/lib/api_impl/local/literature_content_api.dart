import 'package:mango_eater_core/mango_eater_core.dart';

import '../fake_data.dart';

class LocalLiteratureContentApiArgImpl extends LocalLiteratureContentApiArg {}

class LocalLiteratureContentApiResultImpl extends LocalLiteratureContentApiResult {
  @override
  final List<LiteratureContentModel> items;

  LocalLiteratureContentApiResultImpl(this.items);
}

class LocalLiteratureContentApiImpl extends LocalLiteratureContentApi {
  @override
  Future<LocalLiteratureContentApiResult<LiteratureContentModel>> getByArg(LocalLiteratureContentApiArg argument) async {
    return LocalLiteratureContentApiResultImpl([FakeData.fakeContent]);
  }

  @override
  Stream<LocalLiteratureContentApiResult<LiteratureContentModel>> listenByArg(LocalLiteratureContentApiArg argument) {
    return Stream.value(LocalLiteratureContentApiResultImpl([FakeData.fakeContent]));
  }

  @override
  Future<void> saveAll(Iterable<LiteratureContentModel> models) async {}
}

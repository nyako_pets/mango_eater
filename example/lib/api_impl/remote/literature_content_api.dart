import 'package:mango_eater_core/mango_eater_core.dart';

import '../fake_data.dart';

class RemoteLiteratureContentApiArgImpl extends RemoteLiteratureContentApiArg {}

class RemoteLiteratureContentApiResultImpl extends RemoteLiteratureContentApiResult {
  @override
  final List<LiteratureContentModel> items;

  RemoteLiteratureContentApiResultImpl(this.items);
}

class RemoteLiteratureContentApiImpl extends RemoteLiteratureContentApi {
  @override
  Future<RemoteLiteratureContentApiResult<LiteratureContentModel>> getByArg(RemoteLiteratureContentApiArg argument) async {
    return RemoteLiteratureContentApiResultImpl([FakeData.fakeContent]);
  }
}

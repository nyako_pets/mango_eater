import 'package:mango_eater_core/mango_eater_core.dart';

import '../fake_data.dart';

class RemoteLiteratureApiImpl extends RemoteLiteratureApi {
  @override
  Future<LiteratureModel> getById(String id) async {
    return FakeData.fakeLiterature;
  }
}

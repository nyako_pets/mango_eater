import 'package:mango_eater_core/mango_eater_core.dart';

import '../fake_data.dart';

class RemoteLiteratureBranchApiImpl extends RemoteLiteratureBranchApi {
  @override
  Future<List<LiteratureBranchModel>> getByParentId(String id) async {
    return [FakeData.fakeBranch];
  }
}

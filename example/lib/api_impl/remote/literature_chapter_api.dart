import 'package:mango_eater_core/mango_eater_core.dart';

import '../fake_data.dart';

class RemoteLiteratureChapterApiResultImpl extends RemoteLiteratureChapterApiResult {
  @override
  final List<LiteratureChapterModel> items;

  RemoteLiteratureChapterApiResultImpl(this.items);
}

class RemoteLiteratureChapterApiArgImpl extends RemoteLiteratureChapterApiArg {}

class RemoteLiteratureChapterApiImpl extends RemoteLiteratureChapterApi {
  @override
  Future<RemoteLiteratureChapterApiResult<LiteratureChapterModel>> getByArg(RemoteLiteratureChapterApiArg argument) async {
    return RemoteLiteratureChapterApiResultImpl([FakeData.fakeChapter]);
  }
}

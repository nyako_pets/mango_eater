import 'package:mango_eater_core/mango_eater_core.dart';

class FakeData {
  static final fakeLiterature = LiteratureModel(id: '1');

  static final fakeBranch = LiteratureBranchModel(id: '1', parentId: '1');

  static final fakeChapter = LiteratureChapterModel(id: '1', parentId: '1');

  static final fakeContent = LiteratureContentModel(id: '1', parentId: '1');
}
